# Something that looks like an ankle (stllaa)

This is a rough draft project to test feasibility of using servo-driven tension wires to control the ankle pitch and roll of a humanoid robot, inspired by human muscle anatomy.
Uses a ball and socket method to keep the tibia and foot attached

## Images

![](img/stllaa.png)

### Tibia

![](img/tibia.png)

### Foot

![](img/base.png)
